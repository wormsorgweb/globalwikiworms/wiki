This application is developed using github workflow or branch pattern. Developers cannot commit to **main** branch but to **develop** for development. All features are developed in feature branches created form **develop** branch. A feature can then be merged into **develop** using merge request.

![gitFlow__1_](../images/gitFlow__1_.png)

All branches must be named using the following conventions:
- `all branches have to be in lowercase.`
- `feature/{gitlab-card-number}-{description}`: use for new features
- `bug/{gitlab-card-number}-{description}`: use for fixing bugs
- `hotfix/{gitlab-card-number}-{description}`: use for quick fix
- `release/{gitlab-card-number}-{description}`: use for release

Examples:
- `feature/12-form-validation`
- `bug/1-form-style`

Developers should follow the following steps:
- Update the `develop`: `git pull origin develop`
- Create feature branch: `git checkout -b feature/1-form-validation`
- Code :computer: 
- Commit and push your branch
- Raise a Merge Request
- Ask pears for the review
- After passed the review merge the request

**Release**
When develop is merged to main should be squash 