# Definition of Done and Ready

<h3>Definition of done</h3>

- Code reviewed
- Acceptance criteria met
- Pull Request must contain a description and if possible an image of the working implementation.
- All the features described on the Acceptance Criteria must be completed.
- Pull Request description must contain link to the user story from Trello.
- Feature must be tested and approved by qa.

<h3>Definition of ready</h3>

- Story can be developed Independent from other stories.
- One team member can complete the work with minimal or no dependencies on others.
- The story was discussed with the team and they understand what to do.
- The Acceptance criteria is defined.
- The Value of the story is quantified.
- Estimated in Story points by the team.
- Planning poker session done with the team.
- It can be done in One Sprint.
- The team understands how the story can be tested.