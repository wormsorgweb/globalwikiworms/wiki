# Domain-Driven Design (DDD)

Domain-Driven Design is a concept introduced by a programmer Eric Evans in 2004 in his book Domain-Driven Design: Tackling Complexity in Heart of Software.

It is an approach for architecting software design by looking at software in top-down approach. Before discussing topic in detail let’s try to focus some light and understand what is mean by domain in this context.

## What is Domain ?
The word Domain used in context of software development refers to business. In the process of application development, term domain logic or business logic is commonly used. Basically, business logic is area of knowledge around which application logic revolves. The business logic of an application is a set of rules and guidelines that explain how business object should interact with each other to process modeled data.

Suppose we have designed software using all latest tech stack and infrastructure and our software design architecture is fabulous, but when we release this software in market, it is ultimately end user who decides whether our system is great or not. Also if system does not solve business need is of no use to anyone; No matter how pretty it looks or how well architecture its infrastructure. According to Eric Evans, When we are developing software our focus should not be primarily on technology, rather it should be primarily on business. Remember,

It is not the customer’s job to know what they want” – Steve Jobs

Domain-driven design talks about two kinds of design tools, first one is Strategic design tools and another one is Tactical design tools. The programmers or developers usually deal with tactical design tools but if we have knowledge and a good understanding of strategic design tools then it will help us in architecting good software.

## Strategic Design :
The strategic design tools help us to solve all problems that are related to software modeling. It is a design approach that is similar to Object-oriented design where we are forced to think in terms of objects. Herewith strategic design we are forced to think in terms of a context.

## Context :
We can consider this an English word that refers to circumstances of an event, incident, statement, or idea, and in terms of which it’s meaning could be determined.
Apart from Context, Strategic design also talks about Model, Ubiquitous Language, and Bounded Context. These are common terms used in strategic Design of Domain-Driven Design. Let’s understand each one by one.

## Model –
It acts as a core logic and describes selected aspects of domain. it is used to solve problems related to that business.
Ubiquitous Language –
A common language used by all team members to connect all activities of team around domain model. Consider it like using common verbs and nouns for classes, methods, services, and objects while talking with domain experts and team members.
Bounded Context –
It refers to boundary conditions of context. It is a description of a boundary and acts as a threshold within which, a particular domain model is defined and applicable.