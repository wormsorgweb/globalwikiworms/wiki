# MVVM

MVVM is an architectural pattern, invented by Microsoft architects Ken Cooper and Ted Peters. MVVM (Model-View-ViewModel) cleanly separates the business logic of an application from the user interface. The ultimate goal of MVVM architecture is to make the view completely independent from the application logic.

## Model

The model represents the app’s domain model, which can include a data model as well as business and validation logic. It communicates with the ViewModel, and lacks awareness of the View.

## View

The View represents the user interface of the application and holds limited, purely presentational logic that implements visual behavior. The View is completely agnostic to the business logic. In other words, the View is a “dumb” class that never contains data, nor manipulates it directly. It communicates with the ViewModel through data binding and is unaware of the Model.

## ViewModel

The ViewModel is the link between the View and the Model. It implements and exposes public properties and commands that the View uses by way of data binding. If any state changes occur, the ViewModel notifies the View through notification events. 

![MvvM diagram](../images/1_MVVM%20Architecture.png)

### How Does MVVM Architecture Work?

The key to understanding MVVM architecture is understanding how the three key components in MVVM interact with each other. Since the View only communicates with the ViewModel and the ViewModel only communicates with the Model.

All user interaction occurs within the View, which is in charge of detecting the user’s input (mouse clicks, keyboard input) and forwarding it to the ViewModel by way of data binding. Data binding can be implemented with callbacks or properties and constitutes the concrete link between the View and the ViewModel. 

The ViewModel implements the properties and commands to which the view can be bound. These properties and commands define the functionality that the View can offer to the user, although how to display it is entirely up to the View. The ViewModel is also in charge of providing the View with data from the Model classes, which the View can consume. To accomplish this, the ViewModel can expose Model classes directly to the View, in which case the Model class would need to support data binding and change notification events. 

The Models are classes that model the application’s domain. Models encapsulate the application’s data and business logic. They can be considered business objects that have absolutely no relation to the visual aspect of the application

![MvvM diagram 2](../images/MVVMSchema.png)

### Advantages of MVVM

- Easier to develop: Separating the View from the logic makes it possible to have different teams work on different components simultaneously. A team of designers can  focus on the UI while developers work on implementing the logic (ViewModel and Model).
- Easier to test: One of the hardest things to test in an application is the user interface (UI). Because the ViewModel and Model are completely independent from the View, developers can write tests for both without having to use the View. 
- Easier to maintain: The separation between the different components of the application makes the code simpler and cleaner. As a result, the application code is much easier to understand, and therefore, to maintain. It’s easier to understand where we should implement new features and how they connect to the existing pattern. With an MVVM, it’s also easier to implement further architectural patterns (dependency inversion, services and more). 

### Disadvantages of MVVM

- Complexity: MVVM is overkill when it comes to creating simple user interfaces. When working on larger projects, designing the ViewModel in order to get the right amount of generality can be quite difficult.
- Difficult to debug: Because data binding is declarative, it can be harder to debug than traditional, imperative code. 
