# Onion Architecture

Onion Architecture is based on the inversion of control principle. Onion Architecture is comprised of multiple concentric layers interfacing each other towards the core that represents the domain. The architecture does not depend on the data layer as in classic multi-tier architectures, but on the actual domain models.

Domain entities are the core and centre part. Onion architecture is built on a domain model in which layers are connected through interfaces. The idea is to keep external dependencies as far outward as possible where domain entities and business rules form the core part of the architecture.

It provides flexible, sustainable and portable architecture.
Layers are not tightly coupled and have a separation of concerns.
It provides better maintainability as all the code depends on deeper layers or the centre.
Improves overall code testability as unit tests can be created for separate layers without impacting other modules.
Frameworks/technologies can be easily changed without impacting the core domain. e.g. RabbitMQ can be replaced by ActiveMQ, SQL can be replaced by MongoDB.

![Onion Architecture image](../images/onionArchitecture.png)

## Principles

### Dependency

The circles represent different layers of responsibility. In general, the deeper we dive, the closer we get to the domain and business rules. The outer circles represent mechanisms and the inner circles represent core domain logic. The outer layers depend on inner layers and the inner layers are completely unaware of outer circles. Classes, methods, variables, and source code in general belonging to the outer circle depends on the inner circle but not vice versa.

Data formats/structures may vary from layers. Outer layer data formats should not be used by inner layers. E.g. Data formats used in an API can vary from those used in a DB for persistence. Data flow can use data transfer objects. Whenever data crosses layers/boundaries, it should be in a form that is convenient for that layer. E.g. API’s can have DTO’s, DB layer can have Entity Objects depending on how objects stored in a database vary from the domain model.

### Data encapsulation

Each layer/circle encapsulates or hides internal implementation details and exposes an interface to the outer layer. All layers also need to provide information that is conveniently consumed by inner layers. The goal is to minimize coupling between layers and maximize coupling within a vertical slice across layers. We define abstract interfaces at deeper layers and provide their concrete implementation at the outermost layer. This ensures we focus on the domain model without worrying too much about implementation details. We can also use dependency injection frameworks, like Spring, to connect interfaces with implementation at runtime. E.g. Repositories used in the domain and external services used in Application Services are implemented at the infrastructure layer.

![Onion Architecture implementation example](../images/onionArchitectureExample.png)

### Separation of concerns

Application is divided into layers where each layer has a set of responsibilities and addresses separate concerns. Each layer acts as modules/package/namespace within the application.

## Onion Architecture Layers

![Onion Architecture object diagram](../images/onionIlustration.png)

------

![Onion Architecture secuence diagram](../images/onionSecuenceDiagram.png)

### Domain Model/Entities

Domain Entities are the fundamental building block of Domain-Driven Design and they’re used to model concepts of your Ubiquitous Language in code. Entities are Domain concepts that have a unique identity in the problem domain. Domain entities encapsulate attributes and entity behaviour. It is supposed to be independent of specific technologies like databases or web APIs. E.g. In the Orders domain. Order is an entity and has attributes like OrderId, Address, UserInfo, OrderItems, PricingInfo and behaviour like AddOrderItems, GetPricingInfo, ValidateOrder, etc.

![Onion Architecture Domain class example](../images/onionDomain.png)


### Domain services
Domain services are responsible for holding domain logic and business rules. All the business logic should be implemented as a part of domain services. Domain services are orchestrated by application services to serve business use-case. They are NOT typically CRUD services and are usually standalone services. Domain services are responsible for complex business rules like computing pricing and tax information when processing order, Order repository interface for saving and updating order, Inventory Interface for updating information about items purchased, etc.

It consists of algorithms that are essential to its purpose and implement the use cases that are the heart of the application.



### Application services

Application services also referred to as “Use Cases”, are services responsible for just orchestrating steps for requests and should not have any business logic. Application Services interact with other services to fulfil the client’s request. Let’s consider the use case to create an order with a list of items. We first need to calculate the price including tax computation/discounts, etc., save order items and send order confirmation notification to the customer. Pricing computation should be part of the domain service, but orchestration involving pricing computation, checking availability, saving order and notifying users should be part of the application service. The application services can be only invoked by Infrastructure services.


### Infrastructure services

Infrastructure services also referred to as Infrastructure adapters are the outermost layer in onion architecture. These services are responsible for interacting with the external world and do not solve any domain problem. These services just communicate with external resources and don’t have any logic. E.g. External notification Service, GRPC Server endpoint, Kafka event stream adapter, database adapters.

### Observability services

Observability services are responsible for monitoring the application. These services help perform tasks like :

Data collection (metrics, logs, traces) — use mainly libraries/sidecars to collect various data during code execution.
Data storage — use tools that enable central storage of the collected data (sorting, indexing, etc.)
Visualisation — use tools that allow you to visualise the collected data.
Few examples include Splunk, ELK, Grafana, Graphite, Datadog.

### Testing Strategy
Different layers of onion architecture have a different set of responsibilities and accordingly, there are different testing strategies. The testing pyramid is a great framework that lays out the different types of tests. Business rules that belong to the domain model, domain services and application services should be tested via Unit Testing. As we move to the outer layer, it makes more sense to have integration tests in infrastructure services. For our application End to End testing and BDD are the most appropriate testing strategies.

![Onion Architecture Test Strategy example](../images/onionTest.png)


## Application Structure & Layers

| Layers | Package | Module | Depends On | Description | Example | Testing |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| Domain Model | com.example.domain.model | model |  | Responsible for domain identities and business rules | Order validation | Unit Testing |
| Domain Services | com.example.domain.model.service com.example.domain.model.respository | domain | model | responsible for business rules and knowledge, providing interfaces for repositories | Pricing and tax computation DB Repository Interfaces | Unit Testing |
| Application Services | com.example.service | service | domain | responsible for orchestration, authentication, authorisation and excludes business rules, providing interfaces for external services. | Orchestrating Order Creation, Cancellation, Notification | Unit Testing |
| Infrastructure Services | com.example.adapters.db com.example.adapters.stream com.example.adapters.api | adapter-db adapter-stream adapter-api | model, domain, service | acts as an entrypoint for application services like Rest API, Soap API, Event Streams, Queues and scheduled Jobs or provides implementations for interfaces defined in domain or application services | Implementation of External Service Clients. Kafka Consumers,  GRPC, Open API Servers | Unit Testing Integration Testing |
| Application | com.example.app | app | model, domain, service,  adapter-db, adapter-stream, adapter-api | packaging all layers into either web application, command line application. |  | Integration Testing E2E Testing BDD |

------
# Links

- https://medium.com/expedia-group-tech/onion-architecture-deed8a554423#:~:text=Onion%20architecture%20is%20built%20on,flexible%2C%20sustainable%20and%20portable%20architecture.

- https://www.codeguru.com/csharp/understanding-onion-architecture/