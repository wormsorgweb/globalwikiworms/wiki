# Frontend Diagrams

For read the content of .wsd files please install from vscode extensions:
-------
Name: PlantUML

Id: jebbs.plantuml

Description: Rich PlantUML support for Visual Studio Code.

Version: 2.17.5

Publisher: jebbs

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml

-------